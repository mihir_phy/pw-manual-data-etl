import os

STRUCTURED_DATA_BUCKET_NAME = os.environ.get("STRUCTURED_DATA_BUCKET_NAME")
DATA_SCHEMA_TABLE = os.environ.get("DATA_SCHEMA_TABLE")
MANUAL_ENTRIES_TABLE = os.environ.get("MANUAL_ENTRIES_TABLE")
AWS_REGION = "eu-central-1" if os.environ.get("ENV") == "dev" else "eu-west-1"

# STRUCTURED_DATA_BUCKET_NAME = "pw-structured-data-dev"
# DATA_SCHEMA_TABLE = "pw-data-schema-dev"
# MANUAL_ENTRIES_TABLE = "pw-manual-entries-dev"
# AWS_REGION = "eu-central-1"
