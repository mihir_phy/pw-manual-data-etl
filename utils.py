import pandas as pd
import boto3
from io import StringIO
import config as cfg
from datetime import datetime

s3_client = boto3.client("s3")


def get_object_tags(bucket_name: str, object_key: str) -> dict:
    try:
        response = s3_client.get_object_tagging(
            Bucket=bucket_name,
            Key=object_key,
        )
        return response
    except Exception as e:
        print("Exception : ", e)
        return {}


def read_csv_from_s3(bucket_name: str, object_key: str) -> pd.DataFrame:
    try:
        csv_obj = s3_client.get_object(Bucket=bucket_name, Key=object_key)
        body = csv_obj["Body"]
        csv_string = body.read().decode("utf-8")
        return pd.read_csv(StringIO(csv_string))
    except Exception as e:
        print("Exception : ", e)
        return pd.DataFrame()


def get_latest_master_csv(company: str) -> pd.DataFrame:
    # Retrieve the latest dataset file from S3
    get_last_modified = lambda obj: int(obj["LastModified"].strftime("%s"))

    objects_list = s3_client.list_objects_v2(
        Bucket=cfg.STRUCTURED_DATA_BUCKET_NAME, Prefix="{}/".format(company)
    )["Contents"]
    latest_object_key = [
        obj["Key"] for obj in sorted(objects_list, key=get_last_modified)
    ][-1]

    # Read the latest file
    df = read_csv_from_s3(cfg.STRUCTURED_DATA_BUCKET_NAME, latest_object_key)
    return df


def append_data_to_master_csv(
    new_df: pd.DataFrame, master_df: pd.DataFrame
) -> pd.DataFrame:
    try:
        new_df.set_index("date_time", inplace=True)
        master_df.set_index("date_time", inplace=True)
        final_df = pd.concat([master_df, new_df])
        return final_df
    except Exception as e:
        print("Exception : ", e)
        return pd.DataFrame()


def save_csv_to_s3(df: pd.DataFrame, company: str) -> bool:
    try:
        # Write the new file to S3
        timestamp = datetime.now().strftime("%d%m%y_%H%M")
        with StringIO() as csv_buffer:
            df.to_csv(csv_buffer)
            csv_buffer.seek(0)
            file_name = f"{company}/{timestamp}.csv"
            s3_client.put_object(
                Bucket=cfg.STRUCTURED_DATA_BUCKET_NAME,
                Key=file_name,
                Body=csv_buffer.getvalue(),
            )
        return True
    except Exception as e:
        print("Exception : ", e)
        return False
