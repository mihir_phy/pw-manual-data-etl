import re
import boto3
import pandas as pd
import config as cfg

db = boto3.resource("dynamodb", region_name=cfg.AWS_REGION)


def get_data_schema():
    table = db.Table(cfg.DATA_SCHEMA_TABLE)
    response = table.scan()["Items"]

    db_schema = pd.DataFrame(response)
    db_schema.set_index("col", inplace=True)

    return db_schema


def get_columns(company: str, get_all_columns: bool = False):
    db_schema = get_data_schema()
    db_schema = db_schema.reset_index()
    db_schema.drop_duplicates(subset="col", inplace=True)
    db_schema.set_index("col", inplace=True)

    if not get_all_columns:
        # Get only company specific columns
        mask = db_schema["companies"].apply(lambda x: company in x)
        db_schema = db_schema[mask]

    columns = db_schema.to_dict("index")

    # sort the columns by the keys
    columns = {k: columns[k] for k in sorted(columns.keys())}

    return columns


def get_id_columns(company: str):
    """
    Get the id columns of a company
    """
    columns = get_columns(company)
    id_columns = [k for k, v in columns.items() if v["type"] == "id"]
    return id_columns


def validate_new_records(values: dict, company: str) -> bool:
    """
    Validate new entry data
    """

    columns_data = get_columns(company)
    company_columns = columns_data.keys()

    has_non_id_values = False

    for v in values:
        keys = v.keys()
        # check if the id columns are present
        for column in get_id_columns(company):
            if column not in keys or v[column] is None or v[column] == "":
                return False

        # Check if the data is in valid format YYYY-MM-DD HH:MM:SS
        date = v["date_time"]
        if date is not None and date != "":
            if not re.match(r"^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}$", date):
                return False

        # Check if the columns are valid
        for column in keys:
            if column not in company_columns:
                return False

        # Check if at least one non-id column is present
        non_id_columns = [k for k in keys if columns_data[k]["type"] != "id"]
        if len(non_id_columns) == 0:
            return False

        # Check if at least one of the non-id columns has a value
        if not has_non_id_values:
            non_id_columns_values = [v[k] for k in non_id_columns]
            if not all(v is None or v == "" for v in non_id_columns_values):
                has_non_id_values = True

    # Validate if there are non-id values
    if not has_non_id_values:
        return False

    return True
